## Yocto for Raspberry Pi4 ##
-----------------------------------------------------------

### Setup build machine
Before building the image you must install some host package needed by Yocto.
You can find the list in the [official Yocto documentation](https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html#build-host-packages)

### Build the image

Clone this repository and run the following command
```
git submodule update --init --recursive
export TEMPLATECONF=$PWD/layers/meta-plambs-demo/conf/template/rpi
export BUILDDIR=$PWD/build
source layers/poky/oe-init-build-env
bitbake core-image-base
```

### Flash the image on the SDCard

To flash the image use [bmaptool](https://github.com/intel/bmap-tools)
```
cd yocto/build/tmp/deploy/images/raspberrypi4-64/
sudo bmaptool copy core-image-base-raspberrypi4-64.wic.bz2 /dev/sdX
```
assuming /dev/sdX is your sdcard.

